// In this example we have a Warehouse with memory-db. In real world production project,
// the DB would be different (SQL / NOSQL).
function Warehouse() {
    this.items = [];
}

Warehouse.prototype = {
    items: null
};

Warehouse.prototype.add = function(item, quantity) {
    var index = 0;
    var found = false;
    var itemObject = null;

    while (!found && index < this.items.length) {
        itemObject = this.items[index];

        if (itemObject.name === item) {
            itemObject.quantity += quantity;
            found = true;
        }

        index ++
    }

    if (index === this.items.length) {
        this.items.push({name: item, quantity: quantity})
    }
};

Warehouse.prototype.remove = function(item, quantity) {
    if (!this.hasInventory(item, quantity))
        return NaN;

    var itemObject = this.getItem(item);
    itemObject.quantity -= quantity;

    return itemObject.quantity;
};

Warehouse.prototype.getItem = function(item) {
    for (var index = 0; index < this.items.length; index++) {
        itemObject = this.items[index];
        if (itemObject.name === item) {
            return itemObject;
        }
    }

    return null;
};

Warehouse.prototype.getInventory = function(item) {
    var itemObject = this.getItem(item);
    return (itemObject) ? itemObject.quantity : NaN;
};

Warehouse.prototype.hasInventory = function(item, quantity) {
    return (this.getInventory(item) > 0);
};

module.exports = Warehouse;
