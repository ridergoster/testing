function Order(item, quantity) {
    this.item = item;
    this.quantity = quantity;
}

Order.prototype = {
    filled: false,
    quantity: NaN,
    item: null
};

Order.prototype.isFilled = function(){
    return this.filled;
};

Order.prototype.fill = function(warehouse) {
    if (!warehouse.hasInventory(this.item, this.quantity))
        return false;

    warehouse.remove(this.item, this.quantity);
    return this.filled = true;
};

module.exports = Order;
