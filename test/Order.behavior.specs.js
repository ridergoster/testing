// Require assertion and mocking library.
var expect = require('expect.js');
var sinon = require('sinon');

// Require ONLY the SUT.
var Order = require('../sources/Order');

describe('Order Test Cases (Behavior)', function() {
    var STONE = 'Stone';
    var WOOD = 'Wood';

    // Generating Fake Warehouse Object.
    var Warehouse = {
        hasInventory: function() {},
        remove: function() {}
    };

    context('when there is enough quantity in the Warehouse', function() {
        it('should fill the order and decrease the quantity in the warehouse', sinon.test(function() {
            // Setup the SUT and its collaborators.
            var order = new Order(STONE, 50);
            var warehouseMock = sinon.mock(Warehouse);

            // Setting up the expectations, this time based on SUT behavior.
            warehouseMock.expects('hasInventory').once()
                .withArgs(STONE, 50)
                .returns(true);

            warehouseMock.expects('remove').once()
                .withArgs(STONE, 50)
                .returns(0);

            // Exercising the SUT.
            order.fill(Warehouse);

            // Verifying not only the state but the behavior of the SUT.
            expect(order.isFilled()).to.be.true;
            warehouseMock.verify();
            warehouseMock.restore();
        }));
    });

    context('when there is NOT enough quantity in the warehouse', function() {
        it('should NOT fill the order NEITHER decrease the quantitu in the warehouse', sinon.test(function() {
            // Setup the SUT and its collaborators.
            var order = new Order(WOOD, 26);
            var warehouseMock = sinon.mock(Warehouse);

            // Setting up the expectations, again, based on SUT behavior.
            warehouseMock.expects('hasInventory').once()
                .returns(false);

            warehouseMock.expects('remove')
                .never();

            // Exercise the SUT.
            order.fill(Warehouse);

            // Verifying the state & behavior.
            expect(order.isFilled()).to.be.false;
            warehouseMock.verify();
        }));
    });
});