// Require the assertion library.
var expect = require('expect.js');

// Require the SUT and collaborators.
var Order = require('../sources/Order');
var Warehouse = require('../sources/Warehouse');

suite('Order Test Cases (Regular)', function() {
    // Here we're defining some STUBS to simulate Warehouse Inventory.
    var STONE = 'Stone';
    var WOOD = 'Wood';

    var warehouse;
    // Setting up the warehouse.
    setup(function() {
        warehouse = new Warehouse();
        warehouse.add(STONE, 100);
        warehouse.add(WOOD, 25);
    });

    suite('when there is enough quantity in the Warehouse', function() {
        test('should fill the order and decrease the quantity in the warehouse', function() {
            // Setting up the order.
            var order = new Order(STONE, 100);
            // Exercise the SUT.
            order.fill(warehouse);

            // Verify the **state** of the SUT and its collaborators.
            expect(order.isFilled()).to.be.true;
            expect(warehouse.getInventory(STONE)).to.equal(0);
        });
    });

    suite('when there is NOT enough quantity in the warehouse', function() {
        test('should not fill the order neither decrease the quantity in the warehouse', function() {
            // Setting up the order.
            var order = new Order(WOOD, 100);
            // Exercise the SUT.
            order.fill(warehouse);

            // Verify the **state** of the SUT and its collaborators.
            expect(order.isFilled()).to.be.false;
            expect(warehouse.getInventory(WOOD)).to.equal(25);
        });
    });
});